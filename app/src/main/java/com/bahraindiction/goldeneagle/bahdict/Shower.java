package com.bahraindiction.goldeneagle.bahdict;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowContentFrameStats;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;

public class Shower extends Activity {

    private TextView texter;
    private ImageView image;
    static int rate_now = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shower);

        texter = (TextView) findViewById(R.id.text);
        image = (ImageView) findViewById(R.id.imageView3);
        Setter();

        //AdView mAdView = (AdView) findViewById(R.id.adView);
        //  AdRequest adRequest = new AdRequest.Builder().build();
        //  mAdView.loadAd(adRequest);

        Typeface myTypeFace = Typeface.createFromAsset(getAssets(), "fonts/janna.ttf");
        texter = (TextView) findViewById(R.id.text);
        texter.setTypeface(myTypeFace);

        rate();

    }

    public void rate() {

        if (rate_now == 2) {
            AppRater.app_launched(this);
        } else rate_now++;

    }


    public void Setter() {


        switch (AboutFragment.item) {

            case "أوتي":
                texter.setText("مكواة");
                break;
            case "امبرطم":
                ContentSetter(2);
                break;
            case "أم الديفان":
                texter.setText("تصيب الانسان وقد يضيع في الطريق ويذهب إلى مكان غير المقصود.");
                break;
            case "استن":
                ContentSetter(23);
                break;
            case "أجلح أملح":
                ContentSetter(4);
                break;
            case "المكدة":
                ContentSetter(25);
                break;
            case "اللابلة":
                texter.setText("الليلة ما بعد الليلة المقبلة.");
                break;
            case "امبلتع":
                ContentSetter(8);
                break;
            case "بقة":
                texter.setText("الحر الشديد");
                break;
            case "بادقير":
                texter.setText("غطاء القدو");
                break;
            case "بطبطة":
                texter.setText("الدراجة النارية");
                break;
            case "بقعة":
                texter.setText("الاستعجاب من الشيء");
                break;
            case "بجي":
                texter.setText("القصير");
                break;
            case "بقشة":
                texter.setText("الصرة من الثياب ");
                break;
            case "بزخ":
                texter.setText("الضرب على الظهر.");
                break;
            case "تحنقل":
                texter.setText("تعثر واختل توازنه.");
                break;
            case "تغلغص":
                texter.setText("حشر نفسه في مكان ضيق.");
                break;
            case "تفق":
                texter.setText("السلاح الناري");
                break;
            case "تخادن":
                ContentSetter(7);
                break;
            case "ترس":
                texter.setText("ملأ");
                break;
            case "تلح":
                texter.setText("تطلق على الشخص الساذج المخطئ في أتفه الأمور.");
                break;
            case "جكويتو":
                texter.setText("الشيء الرديء الرخيص.");
                break;
            case "جندس":
                texter.setText("انحني");
                break;
            case "جوكم":
                ContentSetter(14);
                break;
            case "جعص":
                ContentSetter(6);
                break;
            case "حمقي":
                texter.setText("الرجل اللذي يغضب لأتفه الأسباب.");
                break;
            case "خريش":
                texter.setText("هو الشخص الغبي , الفيوزات عنده ضاربة.");
                break;
            case "خولجحلة":
                texter.setText("الاستعجاب من الشيء");
                break;
            case "خنة":
                texter.setText("العطر");
                break;
            case "خنينة":
                texter.setText("الرائحة الحلوة.");
                break;
            case "داعوس":
                texter.setText("الممر الضيق بين المباني");
                break;
            case "دبج":
                ContentSetter(3);
                break;
            case "دعله":
                texter.setText("الشخص الغبي والبطيء في الفهم والإدراك.");
                break;
            case "درعم":
                texter.setText("الدخول فجأة بدون استئذان.");
                break;
            case "ديغي":
                texter.setText("الأحمق أو المغفل الأبله.");
                break;
            case "ذلف":
                texter.setText("– أي غادر المكان، لا تقال للصديق أو القريب، بل تقال للشخص الذي يراد مغادرته.");
                break;
            case "ربشه":
                texter.setText("الشخص الذي لايجلس في مكان أبدا و دائم الحركـة");
                break;
            case "راشى":
                texter.setText("الاقناع والمراضاة باللطف والحسنى");
                break;
            case "ريري":
                texter.setText("اللعاب");
                break;
            case "زنجفرة":
                texter.setText("التي شيرت");
                break;
            case "زرنوق":
                texter.setText("الممر الضيق بين المباني الذي يكون مغلقا عادة.");
                break;
            case "زتات":
                texter.setText("تقال للحث على الإسراع في إنجاز الشيء.");
                break;
            case "زقر":
                texter.setText("خاطبه بلغة حادة غاضبة.");
                break;
            case "سابعة":
                texter.setText("الظهر");
                break;
            case "سبعه":
                texter.setText("غسله ");
                break;
            case "سكسبال":
                texter.setText("الثمر الذي يحتوي الفول السوداني.");
                break;
            case "سهدة":
                texter.setText("الهدوء التام");
                break;
            case "سندارة":
                texter.setText("الازعاج الحاد");
                break;
            case "شحقه":
                texter.setText("لماذا");
                break;
            case "شهست":
                texter.setText("ماذا هناك؟");
                break;
            case "شنقايل":
                texter.setText("ما هذا؟");
                break;
            case "شلوشن":
                texter.setText("ورق لاصق ");
                break;
            case "شخاط":
                texter.setText("الكبريت ذو الأعواد.");
                break;
            case "شنهو":
                texter.setText("أي شيء هو؟");
                break;
            case "صخة":
                texter.setText("الهدوء التام");
                break;
            case "صنج":
                ContentSetter(12);
                break;
            case "ضكة":
                texter.setText("الازدحام الشديد");
                break;
            case "طاسة":
                texter.setText("الصحن المجوف الكبير");
                break;
            case "طنقر":
                texter.setText("استشاط غصبا");
                break;
            case "طشت":
                texter.setText("إناء دائري يستعمل للغسيل عادة.");
                break;
            case "طرطور":
                ContentSetter(1);
                break;
            case "عفطي":
                texter.setText("الشخص الصغير الماكر و الشرير.");
                break;
            case "عمبلوص":
                texter.setText("الإسعاف");
                break;
            case "عفر":
                texter.setText("يمكن");
                break;
            case "عباب":
                texter.setText("كثير الكذب.");
                break;
            case "عباله":
                texter.setText("العمل الشاق المتعب");
                break;
            case "عنبوه":
                texter.setText("صيحة استنكار وتعبير عن احتجاج أو تعجب.");
                break;
            case "قرمبع":
                texter.setText("صفة تطلق على السيارة القديمة المتهالكة.");
                break;
            case "قلعه":
                texter.setText("تقال باللام المفخمة، تقال من شخص لآخر قلعتك إذا وقع في مشكلة تم تحذيره منها سابقا. ");
                break;
            case "كوفنه":
                texter.setText("ضربه ضربا مبرحا.");
                break;
            case "كلكجي":
                texter.setText("دائم التحايل وماكر.");
                break;
            case "كفخ":
                ContentSetter(4);
                break;
            case "لفد":
                texter.setText("الشيء الذي يلتصق بالأيدي.");
                break;
            case "لولب":
                texter.setText("صنبور الماء");
                break;
            case "مفهي":
                ContentSetter(17);
                break;
            case "ميش / ماميش":
                texter.setText("موجود || غير موجود");
                break;
            case "ما عندي مقاقة":
                ContentSetter(18);
                break;
            case "محبس":
                texter.setText("الخاتم");
                break;
            case "مجثي":
                texter.setText("الشيء اللذي علاه الغبار والصدأ.");
                break;
            case "محتر":
                texter.setText("الشخص الذي يمتلئ داخله بالعصبية والغيرة.");
                break;
            case "متدوده":
                ContentSetter(16);
                break;
            case "ملوص":
                ContentSetter(10);
                break;
            case "ملوت":
                texter.setText("الملفوف بالشيء");
                break;
            case "ماجلة":
                texter.setText("زاد المنزل من المأكل.");
                break;
            case "محر":
                texter.setText("زاد المنزل من المأكل.");
                break;
            case "مش بوزك":
                ContentSetter(11);
                break;
            case "مكوّك":
                ContentSetter(13);
                break;
            case "نايبة":
                texter.setText("الاستعجاب من الشيء.");
                break;
            case "نغزة":
                ContentSetter(19);
                break;
            case "هايت":
                texter.setText("المنفلت من سلطة الأهل يسرح ويمرح أينما ووقتما شاء. ");
                break;
            case "همزة":
                texter.setText("ما يؤخذ باليد من الرز.");
                break;
            case "همجة":
                ContentSetter(21);
                break;
            case "هيس":
                texter.setText("الإنسان الخبيث والماكر.");
                break;
            case "هتلي":
                texter.setText("الرجل المتسكع الغير ملتزم بعائلة ولا أسرة.");
                break;
            case "وطب":
                ContentSetter(20);
                break;
            case "ودر":
                texter.setText("الشخص الذي تكره مجالسته لسوء طبعه.");
                break;
            case "يبحلق / يخوزر":
                ContentSetter(24);
                break;
            case "يخوره":
                ContentSetter(22);
                break;
            case "يهد":
                texter.setText("الشعور بالألم في الجسم.");
                break;


        }
    }

    public void onResume() {
        super.onResume();
        Setter();


    }

    public void finishedactivity(View view) {
        ImageButton btn = (ImageButton) findViewById(R.id.imageButton);
        btn.setImageResource(R.drawable.backbutclick);
        finish();


    }

    public void ContentSetter(int flag) {


        if (flag == 1) {
            texter.setText(R.string.turtor);
            image.setImageResource(R.drawable.pic05);

        } else if (flag == 2) {
            texter.setText(R.string.embartim);
            image.setImageResource(R.drawable.pic01);

        } else if (flag == 3) {
            texter.setText("ركض ركضا سريعا.");
            image.setImageResource(R.drawable.pic02);

        } else if (flag == 4) {
            texter.setText("الأشعث الغير مهندم اللذي لا يعتني بشكله.");
            image.setImageResource(R.drawable.pic07);

        } else if (flag == 5) {
            texter.setText("الضرب باليد على الوجه والرأس.");
            image.setImageResource(R.drawable.pic08);

        } else if (flag == 6) {
            texter.setText("شديد البخل.");
            image.setImageResource(R.drawable.pic09);

        } else if (flag == 7) {
            texter.setText("التخطيط لخطة أو عمل ما.");
            image.setImageResource(R.drawable.pic03);

        } else if (flag == 8) {
            texter.setText("المكثر في الحديث مع مهارة في اختيار مفرداته.");
            image.setImageResource(R.drawable.pic06);

        } else if (flag == 10) {
            texter.setText("المخادع المكار.");
            image.setImageResource(R.drawable.pic10);

        } else if (flag == 11) {
            texter.setText("امسح فمك، وتعني اقطع الأمل في الحصول على طلبك فهو مستحيل.");
            image.setImageResource(R.drawable.pic11);

        } else if (flag == 12) {
            texter.setText("تطلق على الشخص القذر ذو الرائحة الكريهة.");
            image.setImageResource(R.drawable.pic12);

        } else if (flag == 13) {
            texter.setText("الشخص المتطاول الذي تم شحنه بما يجعله في حالة مزاجية سيئة مع شخص ما.");
            image.setImageResource(R.drawable.pic13);

        } else if (flag == 14) {
            texter.setText("خاطر أو جازف بالقيام بعمل ما قد ينجح أو يفشل");
            image.setImageResource(R.drawable.pic14);

        } else if (flag == 15) {
            texter.setText("دائم التحايل وماكر.");
            image.setImageResource(R.drawable.pic15);

        } else if (flag == 16) {
            texter.setText("الشخص الحيران أو المشغول فكره.");
            image.setImageResource(R.drawable.pic16);

        } else if (flag == 17) {
            texter.setText("الشخص الذي يسرح دائما وقليل الانتباه لشرود ذهنه.");
            image.setImageResource(R.drawable.pic17);

        } else if (flag == 18) {
            texter.setText("ليس لي مزاج");
            image.setImageResource(R.drawable.pic18);

        } else if (flag == 19) {
            texter.setText("لها معنيين، الأول هو اللكز أو النخز بالشوكة ونحوه، والثاني هو توجيه الكلام لشخص بينما المقصود شخص آخر.");
            image.setImageResource(R.drawable.pic19);

        } else if (flag == 20) {
            texter.setText("ثقيل النفس والحديث.");
            image.setImageResource(R.drawable.pic20);

        } else if (flag == 21) {
            texter.setText("المبلغ الكبير من المال.");
            image.setImageResource(R.drawable.pic21);

        } else if (flag == 22) {
            texter.setText("لا يتكلم عن شيء او موضوع محدد.");
            image.setImageResource(R.drawable.pic22);

        } else if (flag == 23) {
            texter.setText("استشاط أو أسرع في المسير.");
            image.setImageResource(R.drawable.pic23);

        } else if (flag == 24) {
            texter.setText("يطيل النظر الى شيء أو شخص.");
            image.setImageResource(R.drawable.pic24);

        } else if (flag == 25) {
            texter.setText("ما يتحصل عليه لقاء الكد والتعب، كالراتب.");
            image.setImageResource(R.drawable.pic22);

        }
    }
}